<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {


    /**
     * fungsi validasi input
     * Rules : Dideklarasikan pada masing masing Model
     * 
     */
    public function validate()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters(
            '<small class="form-text text-danger">',
            '</small>'
        );

        $validationRules = $this->getValidationRules();

        $this->form_validation->set_rules($validationRules);

        return $this->form_validation->run();
    }


 /**
     * Menampilkan banyak data dari hasil query dan kondisi
     * Hasil Akhir chain method
     */
    public function get()
    {
        return $this->db->get($this->table)->result();
    }

    /**
     * Menampilkan nilai jumlah data dari hasil query dan kondisi
     * Hasil Akhir chain method
     */
    public function count()
    {
        return $this->db->count_all_results($this->table);
    }

    /**
     * Menyimpan data baru ke dalam suatu table
     * 
     * @param [type] $data
     */
    public function create($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Mengubah data yang ada pada suatu table dengan data baru
     * 
     * @param [type] $data
     */
    public function update($data)
    {
        return $this->db->update($this->table, $data);
    }

    /**
     * Menghapus suatu data dari hasil query dan kondisi
     * 
     */
    public function delete()
    {
        $this->db->delete($this->table);
        return $this->db->affected_rows();
    }



    // protected $perPage = 5;

    public function getDefaultValues()
    {
        return [
            'id_product'   => '',
            'name'          => '',
            'price'         => '',
        ];
    }

    public function getValidationRules()
    {
        $validationRules = [
            [
                'field' => 'id_product',
                'label' => 'product',
                'rules' => 'required'
            ],
            [
                'field' => 'name',
                'label' => 'nama product',
                'rules' => 'required'
            ],
            [
                'field' => 'price',
                'label' => 'Harga',
                'rules' => 'trim|required|numeric'
            ],
            
        ];

        return $validationRules;
    }

   

}

/* End of file Product_model.php */
